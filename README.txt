
Readme file for og_author.module for drupal. You need to have og.module installed.

This module adds a new select field with in the post of a group to set the group which is authoring it.

You can use the array $node->og_author to show the information of the authoring group within your theme.
The keys for that  array are:

nid: nid of the post
gid: nid of the authoring group
name: name of the group (node title)